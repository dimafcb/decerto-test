import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ANIMATION_EXPAND } from './const/animations';
import { PRODUCTS_TEST_DATA } from './const/products';
import { Product } from './model/product';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ANIMATION_EXPAND],
})
export class AppComponent {
  products: Product[] = PRODUCTS_TEST_DATA;
  expanded = true;

  productsFormControl = new FormControl({ value: undefined, disabled: false });

  constructor() {
    this.productsFormControl.valueChanges.subscribe((val) => {
      console.log('Wybrano produkt..', val);
    });
  }

  expand(): void {
    this.expanded = !this.expanded;
  }
}
