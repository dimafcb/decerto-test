import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ItemSelectComponent } from './components/item-select/item-select.component';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, ReactiveFormsModule, FormsModule],
      declarations: [AppComponent, ItemSelectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should render header "Wybór Produktów"', () => {
    const template = fixture.nativeElement;
    expect(template.querySelector('.title').textContent).toEqual('Wybór Produktów');
  });

  it('should call "expand" method on "expand-icon" click', fakeAsync(() => {
    spyOn(component, 'expand');
    const template = fixture.nativeElement;
    const expandIcon = template.querySelector('.expand-icon');
    expandIcon.click();
    tick();
    expect(component.expand).toHaveBeenCalled();
  }));

  it('should add class "expanded" to "content" div after "expand()" method was called', fakeAsync(() => {
    component.expanded = false;
    component.expand();
    tick();
    const template = fixture.nativeElement;
    const content: HTMLDivElement = template.querySelector('.content');
    expect(content.classList).toContain('expanded');
  }));
});
