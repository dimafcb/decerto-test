import { CommonModule } from '@angular/common';
import { forwardRef, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PRODUCTS_TEST_DATA } from './../../const/products';
import { Product } from './../../model/product';
import { ItemSelectComponent } from './item-select.component';

describe('ItemSelectComponent', () => {
  let component: ItemSelectComponent<Product>;
  let fixture: ComponentFixture<ItemSelectComponent<Product>>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, CommonModule, BrowserAnimationsModule],
      declarations: [ItemSelectComponent],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          multi: true,
          useExisting: forwardRef(() => ItemSelectComponent),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have value of some item after clicking on it', fakeAsync(() => {
    const TARGET_ITEM_INDEX = 2;
    component.items = PRODUCTS_TEST_DATA;
    tick();
    fixture.detectChanges();
    const items = fixture.nativeElement.querySelectorAll('.item'); // get all items
    items.item(TARGET_ITEM_INDEX).click();
    expect(component.selectedItem).toBe(component.items[TARGET_ITEM_INDEX]);
  }));
});
