import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Item } from './../../model/item';

@Component({
  selector: 'item-select',
  templateUrl: './item-select.component.html',
  styleUrls: ['./item-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ItemSelectComponent),
    },
  ],
})
export class ItemSelectComponent<T extends Item> implements ControlValueAccessor {
  @Input() items: T[] = [];

  disabled = false;
  selectedItem: T | undefined = undefined;

  onChange = (item: T | undefined) => {};
  onTouched = () => {};

  constructor() {}

  // CVA implementing

  writeValue(item: T | undefined): void {
    this.selectedItem = item;
  }

  registerOnChange(onChange: any): void {
    this.onChange = onChange;
  }

  registerOnTouched(onTouched: any): void {
    this.onTouched = onTouched;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  // view methods

  itemClick(item: T): void {
    if (this.disabled) {
      return;
    }
    if (this.selectedItem === item) {
      // click on selected item perform deselecting
      this.writeValue(undefined);
    } else {
      this.writeValue(item);
    }
    this.onChange(this.selectedItem);
    this.onTouched();
  }
}
