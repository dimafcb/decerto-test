// filling array with generated products
// note, variable 'i' is already incremented during assignment 'id: ++i', so next properies can use just 'i'
export const PRODUCTS_TEST_DATA = [...Array(6)].map((v, i) => ({ id: ++i, name: `Produkt ${i}`, description: `Produkt ${i} - najlepszy` }));
