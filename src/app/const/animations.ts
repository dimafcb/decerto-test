import { animate, state, style, transition, trigger } from '@angular/animations';

export const ANIMATION_EXPAND = trigger('animationExpand', [
  state('true', style({ height: '*' })),
  state('false', style({ height: '0px', overflow: 'hidden' })),
  transition('* => *', animate('200ms ease-in-out')),
]);
