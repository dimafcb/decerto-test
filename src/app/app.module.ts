import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ItemSelectComponent } from './components/item-select/item-select.component';

@NgModule({
  declarations: [AppComponent, ItemSelectComponent],
  imports: [BrowserModule, CommonModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
